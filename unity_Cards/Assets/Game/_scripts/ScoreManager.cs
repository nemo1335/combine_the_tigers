using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    private int _scoreMultiplier;

    private void Start()
    {
        _scoreMultiplier = 1;

        GameEvents.Instance.OnScoreIncreased += IncreaseScore;
        GameEvents.Instance.OnScoreDecreased += DecreaseScore;
        GameEvents.Instance.OnDoubleScore += DoubleScore;
    }

    private void OnDestroy()
    {
        GameEvents.Instance.OnScoreIncreased -= IncreaseScore;
        GameEvents.Instance.OnScoreDecreased -= DecreaseScore;
        GameEvents.Instance.OnDoubleScore -= DoubleScore;
    }

    private void IncreaseScore(int value)
    {
        SLS.Data.GameData.TotalCoinsAmount.Value += value * _scoreMultiplier;
    }

    private void DecreaseScore(int value)
    {
        SLS.Data.GameData.TotalCoinsAmount.Value -= value;
    }

    private void DoubleScore()
    {
        _scoreMultiplier = 2;
    }
}
