using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class ScenesController : MonoBehaviour
{
    [SerializeField] private Button playButton;
    [SerializeField] private GameObject loadingpanel;
    [SerializeField] private Canvas canvas;
    [SerializeField] private Text loadProgress;
    private AsyncOperation _loadingSceneOperation;
    private AsyncOperation _unloadingSceneOperation;
    private float _loadProgress;
    private Tween _rotationTween;

    private void Start()
    {
        GameEvents.Instance.OnPlayPressed += LoadGameScene;
        GameEvents.Instance.OnHomePressed += LoadHomeScene;
        GameEvents.Instance.OnGameRestarted += ReloadGameScene;

        ChangeLoadProgress();
        RotatePlayButton();
    }

    private void OnDestroy()
    {
        GameEvents.Instance.OnPlayPressed -= LoadGameScene;
        GameEvents.Instance.OnHomePressed -= LoadHomeScene;
        GameEvents.Instance.OnGameRestarted -= ReloadGameScene;
    }


    private void LoadHomeScene()
    {
        StartCoroutine(UnloadScene(ScenesInBuild.GameScene));
        StartCoroutine(LoadScene(ScenesInBuild.HomeScene));
        SoundController.Instance.PlayMainBackgroundMusic();
    }

    private void LoadGameScene()
    {
        StartCoroutine(UnloadScene(ScenesInBuild.HomeScene));
        SoundController.Instance.StopBackgroundMusic();
        StartCoroutine(LoadScene(ScenesInBuild.GameScene));
    }

    private void ReloadGameScene()
    {
        StartCoroutine(UnloadScene(ScenesInBuild.GameScene));
        StartCoroutine(LoadScene(ScenesInBuild.GameScene));
    }

    private IEnumerator LoadScene(ScenesInBuild scene)
    {
        var sceneIndex = (int)scene;
        yield return _loadingSceneOperation = SceneManager.LoadSceneAsync(sceneIndex, LoadSceneMode.Additive);
        SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(sceneIndex));
    }

    private IEnumerator UnloadScene(ScenesInBuild scene)
    {
        var sceneIndex = (int)scene;
        var sceneToUnload = SceneManager.GetSceneByBuildIndex(sceneIndex);
        if (sceneToUnload.IsValid())
        {
            yield return _unloadingSceneOperation = SceneManager.UnloadSceneAsync(sceneToUnload);
        }
    }

    private void RotatePlayButton()
    {
        _rotationTween = playButton.GetRectTransform().DORotate(new Vector3(0f, 0f, -360f), 4, RotateMode.FastBeyond360)
            .SetEase(Ease.Linear)
            .SetLoops(-1, LoopType.Incremental);
    }

    private void ChangeLoadProgress()
    {
        DOTween.To(() => _loadProgress, x => _loadProgress = x, 100f, 4f)
            .SetEase(Ease.Linear)
            .OnUpdate(() => UpdateLoadProgress())
            .OnComplete(() =>
            {
                KillRotationTween();
                playButton.onClick.AddListener(LoadHomeScene);
                playButton.onClick.AddListener(DestroyCanvas);
                loadingpanel.SetInactive();
            });
    }

    private void DestroyCanvas()
    {
        if (canvas.gameObject != null)
        {
            Destroy(canvas.gameObject);
        }
    }

    private void UpdateLoadProgress()
    {
        int intValue = (int)_loadProgress;
        string textToShow = intValue + "%";

        loadProgress.text = textToShow;
    }

    private void KillRotationTween()
    {
        _rotationTween.Kill();
    }

    public enum ScenesInBuild
    {
        MainScene,
        HomeScene,
        GameScene
    }
}
