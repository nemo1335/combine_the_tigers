using System;

[Serializable]
public class GameDataSaves
{
    public StoredValue<int> TotalCoinsAmount;

    public StoredValue<int> SelectCardPrice;
    public StoredValue<int> SkipCardPrice;
    public StoredValue<int> CombineJokersPrice;
    public StoredValue<int> CatchTigerPrice;


    public StoredValue<int> DoubleScorePrice;
    public StoredValue<int> BonusLifePrice;
    public StoredValue<int> BonusTimePrice;
    public StoredValue<int> SuperVisionPrice;

    public StoredValue<bool> TutorialCompleted;

    public GameDataSaves()
    {
        TotalCoinsAmount = new StoredValue<int>(1000);
        
        SelectCardPrice = new StoredValue<int>(50);
        SkipCardPrice = new StoredValue<int>(70);
        CombineJokersPrice = new StoredValue<int>(200);
        CatchTigerPrice = new StoredValue<int>(400);


        BonusLifePrice = new StoredValue<int>(600);
        DoubleScorePrice = new StoredValue<int>(1500);
        BonusTimePrice = new StoredValue<int>(1200);
        SuperVisionPrice = new StoredValue<int>(1000);
        
        TutorialCompleted = new StoredValue<bool>(false);
    }
}