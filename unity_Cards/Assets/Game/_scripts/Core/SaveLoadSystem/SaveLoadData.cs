﻿using System;

[Serializable]
public class SaveLoadData
{
    public SettingsData Settings;

    public GameDataSaves GameData;

    public StoredValue<int> DoubleScoreAbilityAmount;
    public StoredValue<int> BonusLifeAbilityAmount;
    public StoredValue<int> BonusTimeAbilityAmount;
    public StoredValue<int> SuperVisionAbilityAmount;

    public SaveLoadData()
    {
        Settings = new SettingsData();

        GameData = new GameDataSaves();
        
        DoubleScoreAbilityAmount = new StoredValue<int>(2);
        BonusLifeAbilityAmount = new StoredValue<int>(2);
        BonusTimeAbilityAmount = new StoredValue<int>(2);
        SuperVisionAbilityAmount = new StoredValue<int>(2);
    }
}