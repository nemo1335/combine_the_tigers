using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class SuperVision : BaseAbility
{
    [SerializeField] private Button superVisionButton;
    [SerializeField] private Text abilityAmountText;
    [SerializeField] private GameObject cooldownPanel;
    [SerializeField] private Text cooldownTimer;
    [SerializeField] private float cooldownAbility = 40f;
    [SerializeField] private float durationAbility = 10f;
    private bool _isAbilityAvailable = false;

    private void Start()
    {
        SetAmount(SLS.Data.SuperVisionAbilityAmount.Value);
        CheckAbilityAvailable();
        superVisionButton.onClick.AddListener(ActivateAbility);
        SLS.Data.SuperVisionAbilityAmount.OnValueChanged += SetAmount;
    }

    private void OnDestroy()
    {
        SLS.Data.SuperVisionAbilityAmount.OnValueChanged -= SetAmount;
    }

    protected override void ActivateAbility()
    {
        if (_isAbilityAvailable)
        {
            GameEvents.Instance.SuperVisionActivated(true);
            SLS.Data.SuperVisionAbilityAmount.Value--;
            _isAbilityAvailable = false;
            superVisionButton.interactable = false;

            StartCoroutine(CountAbilityDuration(durationAbility));
            StartCoroutine(CountAbilityCooldown());
        }
    }

    private IEnumerator CountAbilityDuration(float duration)
    {
        yield return new WaitForSeconds(duration);
        GameEvents.Instance.SuperVisionActivated(false);
    }

    private IEnumerator CountAbilityCooldown()
    {
        cooldownPanel.SetActive();
        while (cooldownAbility > 0)
        {
            cooldownAbility -= Time.deltaTime;
            DisplayTime(cooldownAbility);
            yield return null;
        }
        cooldownPanel.SetInactive();
        CheckAbilityAvailable();
    }

    private void DisplayTime(float timeToDisplay)
    {
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);

        cooldownTimer.text = seconds.ToString();
    }

    private bool CheckAbilityAvailable()
    {
        return _isAbilityAvailable = base.CheckAbilityAvailable(_isAbilityAvailable, SLS.Data.SuperVisionAbilityAmount.Value, superVisionButton);
    }

    private void SetAmount(int newAmount)
    {
        base.SetAbilityAmount(abilityAmountText, newAmount);
    }
}
