using UnityEngine;
using UnityEngine.UI;

public class BonusLife : BaseAbility
{
    [SerializeField] private Button bonusLifeButton;
    [SerializeField] private Text abilityAmountText;
    private bool _isAbilityAvailable;

    private void Start()
    {
        SetAmount(SLS.Data.BonusLifeAbilityAmount.Value);
        CheckAbilityAvailable();

        bonusLifeButton.onClick.AddListener(ActivateAbility);

        SLS.Data.BonusLifeAbilityAmount.OnValueChanged += SetAmount;
    }

    private void OnDestroy()
    {
        SLS.Data.BonusLifeAbilityAmount.OnValueChanged -= SetAmount;
    }

    protected override void ActivateAbility()
    {
        if (_isAbilityAvailable)
        {
            GameEvents.Instance.BonusLifeActivated();
            SLS.Data.BonusLifeAbilityAmount.Value--;
            _isAbilityAvailable = false;
            bonusLifeButton.interactable = false;
        }
    }

    private bool CheckAbilityAvailable()
    {
        return _isAbilityAvailable = base.CheckAbilityAvailable(_isAbilityAvailable, SLS.Data.BonusLifeAbilityAmount.Value, bonusLifeButton);
    }

    private void SetAmount(int newAmount)
    {
        base.SetAbilityAmount(abilityAmountText, newAmount);
    }
}
