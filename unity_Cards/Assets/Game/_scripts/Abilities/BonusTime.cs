using UnityEngine;
using UnityEngine.UI;

public class BonusTime : BaseAbility
{
    [SerializeField] private Button bonusTimeButton;
    [SerializeField] private Text abilityAmountText;
    private bool _isAbilityAvailable;

    private void Start()
    {
        SetAmount(SLS.Data.BonusTimeAbilityAmount.Value);
        CheckAbilityAvailable();

        bonusTimeButton.onClick.AddListener(ActivateAbility);

        SLS.Data.BonusTimeAbilityAmount.OnValueChanged += SetAmount;
    }

    private void OnDestroy()
    {
        SLS.Data.BonusTimeAbilityAmount.OnValueChanged -= SetAmount;
    }

    protected override void ActivateAbility()
    {
        if (_isAbilityAvailable)
        {
            GameEvents.Instance.BonusTimeActivated();
            SLS.Data.BonusTimeAbilityAmount.Value--;
            _isAbilityAvailable = false;
            bonusTimeButton.interactable = false;
            CheckAbilityAvailable();
        }
    }

    private bool CheckAbilityAvailable()
    {
        return _isAbilityAvailable = base.CheckAbilityAvailable(_isAbilityAvailable, SLS.Data.BonusTimeAbilityAmount.Value, bonusTimeButton);
    }

    private void SetAmount(int newAmount)
    {
        base.SetAbilityAmount(abilityAmountText, newAmount);
    }
}
