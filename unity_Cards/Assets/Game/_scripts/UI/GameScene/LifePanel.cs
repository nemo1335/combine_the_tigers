using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifePanel : MonoBehaviour
{
    [SerializeField] private GameObject lifePrefab;
    [SerializeField] private Sprite emptyLife;
    private List<GameObject> spawnedLife = new List<GameObject>();
    private Image _healthImage;
    private int _lifeCount = 3;
    private int _lastLifeIndex;

    private void Start()
    {
        InstantiateLifes();

        GameEvents.Instance.OnLifeLosed += DecreaseLifesCount;
        GameEvents.Instance.OnGameRestarted += ReinstantiateLifes;
        GameEvents.Instance.OnBonusLife += IncreaseLifesCount;
    }

    private void OnDestroy()
    {
        GameEvents.Instance.OnLifeLosed -= DecreaseLifesCount;
        GameEvents.Instance.OnGameRestarted -= ReinstantiateLifes;
        GameEvents.Instance.OnBonusLife -= IncreaseLifesCount;
    }

    private void InstantiateLifes()
    {
        for (int i = 0; i < _lifeCount; i++)
        {
            var health = Instantiate(lifePrefab, this.transform);
            spawnedLife.Add(health);
        }
        _lastLifeIndex = spawnedLife.Count - 1;
    }

    private void IncreaseLifesCount()
    {
        var health = Instantiate(lifePrefab, this.transform);
        health.transform.SetAsFirstSibling();
        spawnedLife.Insert(0, health);
        _lastLifeIndex++;
        _lifeCount++;
    }

    private void DecreaseLifesCount()
    {
        var lifeImage = spawnedLife[_lastLifeIndex].GetComponent<Image>();
        lifeImage.sprite = emptyLife;
        _lastLifeIndex--;
        _lifeCount--;
        if (_lifeCount == 0)
        {
            GameEvents.Instance.AllLifesLosed();
        }
    }

    private void ReinstantiateLifes()
    {
        foreach (GameObject life in spawnedLife)
        {
            Destroy(life);
        }
        spawnedLife.Clear();

        _lifeCount = 3;
        InstantiateLifes();
    }
}
