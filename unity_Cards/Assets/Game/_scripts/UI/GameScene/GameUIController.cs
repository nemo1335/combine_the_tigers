using UnityEngine;
using UnityEngine.UI;

public class GameUIController : MonoBehaviour
{
    [Header("Panels")]
    [SerializeField] private GameObject pausePanel;
    [SerializeField] private GameObject settingsPanel;
    [SerializeField] private DelayPanel delayPanel;
    [SerializeField] private GameObject losePanel;
    [SerializeField] private TutorialPanel tutorialPanel;

    [Header("Buttons")]
    [SerializeField] private Button pauseButton;
    [SerializeField] private Button settingsButton;
    [SerializeField] private Button closeSettingsButton;
    [SerializeField] private Button resumeButton;
    [SerializeField] private Button restartButton;

    [Header("Texts")]
    [SerializeField] private Text gameCoinsAmountText;
    [SerializeField] private Text loseCoinsAmountText;
    [SerializeField] private Text earnedCoinsAmountText;

    private int _startCoinsAmount;

    private void Start()
    {
        SetCoinsAmountText(SLS.Data.GameData.TotalCoinsAmount.Value);
        _startCoinsAmount = SLS.Data.GameData.TotalCoinsAmount.Value;

        pauseButton.onClick.AddListener(PauseGame);
        resumeButton.onClick.AddListener(ResumeGame);
        restartButton.onClick.AddListener(RestartGame);
        settingsButton.onClick.AddListener(OpenSettings);
        closeSettingsButton.onClick.AddListener(CloseSettings);

        SLS.Data.GameData.TotalCoinsAmount.OnValueChanged += SetCoinsAmountText;

        GameEvents.Instance.OnGameLosed += PlayerLose;
        GameEvents.Instance.OnDelayPanelActivated += OpenDelayPanel;
        GameEvents.Instance.OnDelayPanelActivated += CloseTutorialPanel;

        if (!SLS.Data.GameData.TutorialCompleted.Value)
        {
            tutorialPanel.gameObject.SetActive();
        }
        else
        {
            OpenDelayPanel();
        }
    }

    private void OnDestroy()
    {
        SLS.Data.GameData.TotalCoinsAmount.OnValueChanged -= SetCoinsAmountText;

        GameEvents.Instance.OnGameLosed -= PlayerLose;
        GameEvents.Instance.OnDelayPanelActivated -= OpenDelayPanel;
        GameEvents.Instance.OnDelayPanelActivated -= CloseTutorialPanel;
    }

    private void OpenDelayPanel()
    {
        delayPanel.SetActive();
    }

    private void CloseTutorialPanel()
    {
        tutorialPanel.gameObject.SetInactive();
    }

    private void PauseGame()
    {
        GameEvents.Instance.PlayerStopGame();
        pausePanel.SetActive();
    }

    private void ResumeGame()
    {
        pausePanel.SetInactive();
        OpenDelayPanel();
    }

    private void OpenSettings()
    {
        settingsPanel.SetActive();
    }

    private void CloseSettings()
    {
        settingsPanel.SetInactive();
    }

    private void PlayerLose()
    {
        earnedCoinsAmountText.text = (SLS.Data.GameData.TotalCoinsAmount.Value - _startCoinsAmount).ToString();
        losePanel.SetActive();
    }

    private void RestartGame()
    {
        GameEvents.Instance.PlayerRestartGame();
        losePanel.SetInactive();
        OpenDelayPanel();
    }

    private void SetCoinsAmountText(int newValue)
    {
        gameCoinsAmountText.text = newValue.ToString();
        loseCoinsAmountText.text = newValue.ToString();
    }
}
