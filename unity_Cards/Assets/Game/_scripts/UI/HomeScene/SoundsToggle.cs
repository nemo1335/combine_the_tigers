using UnityEngine;
using UnityEngine.UI;

public class SoundsToggle : MonoBehaviour
{
    [SerializeField] private Sprite onToggleSprite;
    [SerializeField] private Sprite offToggleSprite;
    [SerializeField] private Button button;
    [SerializeField] private Image toggleIcon;
    private Image _buttonBackground;

    private void Start()
    {
        SetToggleSprite(SLS.Data.Settings.SoundsMuted.Value);
        button.onClick.AddListener(ChangeSoundsToggleState);
    }

    private void ChangeSoundsToggleState()
    {
        SoundController.Instance.ToggleSounds();
        SetToggleSprite(SLS.Data.Settings.SoundsMuted.Value);
    }

    private void SetToggleSprite(bool soundsMuted)
    {
        if (soundsMuted)
        {
            toggleIcon.sprite = offToggleSprite;
        }
        else
        {
            toggleIcon.sprite = onToggleSprite;
        }
    }
}
