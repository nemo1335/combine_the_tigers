using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.EventSystems;

public class ShopItemView : MonoBehaviour, IPointerClickHandler
{
    public event Action<ShopItemView> Click;
    [SerializeField] private Sprite background;
    [SerializeField] private Image abilityImage;
    [SerializeField] private Text abilityAmount;
    [SerializeField] private Text abilityPrice;
    [SerializeField] private Text abilityName;
    private Image _backgroundImage;

    public ShopItem Item { get; private set; }

    public int Price => Item.Price;

    public void Initialize(ShopItem item)
    {
        _backgroundImage = GetComponent<Image>();
        _backgroundImage.sprite = background;
        Item = item;

        abilityName.text = item.Name;
        abilityImage.sprite = item.Icon;
        abilityPrice.text = item.Price.ToString();
        SetAbilityAmount(item.AbilityType);
    }

    public void SetAbilityAmount(Abilities abilities)
    {
        switch (abilities)
        {
            case Abilities.BonusLife:
                abilityAmount.text = SLS.Data.BonusLifeAbilityAmount.Value.ToString();
                break;
            case Abilities.DoubleScore:
                abilityAmount.text = SLS.Data.DoubleScoreAbilityAmount.Value.ToString();
                break;
            case Abilities.BonusTime:
                abilityAmount.text = SLS.Data.BonusTimeAbilityAmount.Value.ToString();
                break;
            case Abilities.SuperVision:
                abilityAmount.text = SLS.Data.SuperVisionAbilityAmount.Value.ToString();
                break;
        }
    }

    public void OnPointerClick(PointerEventData eventData) => Click?.Invoke(this);
}
