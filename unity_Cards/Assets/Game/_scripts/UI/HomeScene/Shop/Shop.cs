using System.Linq;
using UnityEngine;

public class Shop : MonoBehaviour
{
    [SerializeField] private ShopContent contentItems;
    [SerializeField] private ShopPanel shopPanel;

    private void Start()
    {
        shopPanel.Show(contentItems.AbilityItems.Cast<ShopItem>());
    }
}
