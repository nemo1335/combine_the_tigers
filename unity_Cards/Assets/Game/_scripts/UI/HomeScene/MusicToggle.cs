using UnityEngine;
using UnityEngine.UI;

public class MusicToggle : MonoBehaviour
{
    [SerializeField] private Sprite onToggleSprite;
    [SerializeField] private Sprite offToggleSprite;
    [SerializeField] private Button button;
    [SerializeField] private Image toggleIcon;
    private Image _buttonBackground;

    private void Start()
    {
        SetToggleSprite(SLS.Data.Settings.MusicMuted.Value);
        button.onClick.AddListener(ChangeMusicToggleState);
    }

    private void ChangeMusicToggleState()
    {
        SoundController.Instance.ToggleMusic();
        SetToggleSprite(SLS.Data.Settings.MusicMuted.Value);
    }

    private void SetToggleSprite(bool musicMuted)
    {
        if (musicMuted)
        {
            toggleIcon.sprite = offToggleSprite;
        }
        else
        {
            toggleIcon.sprite = onToggleSprite;
        }
    }
}
