using UnityEngine;
using System;
using DG.Tweening;

[RequireComponent(typeof(CardItemView))]
public class CardSwipe : MonoBehaviour
{
    [SerializeField] private AudioClip takeCardSound;
    [SerializeField] private AudioClip discardCardSound;
    [SerializeField] private AudioClip putCardSound;
    private Transform _playTable;
    private CardItemView _cardItemView;
    private Vector2 _mousePosition;
    private bool _isClickDown;
    private bool _isSuperVision = false;
    private bool _isMoveble = true;
    private Vector2 _mouseDownPosition;
    private Vector2 _mouseUpPosition;
    public event Action<CardItemView> OnCardSelected;
    public event Action<CardItemView> OnCardDiscarded;

    private void Start()
    {
        _mousePosition = transform.position;
        _cardItemView = GetComponent<CardItemView>();

        GameEvents.Instance.OnSuperVision += CheckSuperVisionAbility;
    }

    private void OnDestroy()
    {
        GameEvents.Instance.OnSuperVision -= CheckSuperVisionAbility;
    }

    private void Update()
    {
        GetMousePosition();
    }

    private void FixedUpdate()
    {
        if (Timer.IsInGame)
        {
            if (_isMoveble)
            {
                Move();
            }
        }
    }

    private void Move()
    {
        transform.position = _mousePosition;
    }

    private void OnMouseDown()
    {
        if (Timer.IsInGame)
        {
            if (_isMoveble)
            {
                SoundController.Instance.PlayEffectSound(takeCardSound);
                _mouseDownPosition = transform.position;

                SuperVision();
                _isClickDown = true;
            }
        }
    }

    private void OnMouseUp()
    {
        if (Timer.IsInGame)
        {
            if (_isMoveble)
            {
                _isClickDown = false;
                _mouseUpPosition = transform.position;
                CalculateDirectionOffset();
                _isMoveble = false;
            }
        }
    }

    private void CalculateDirectionOffset()
    {
        if (_cardItemView.Card.CardType != Cards.BlueJoker)
        {
            GameEvents.Instance.PutFirstCard();
        }
        else
        {
            GameEvents.Instance.PutSecondCard();
        }

        var difference = _mouseDownPosition.y - _mouseUpPosition.y;
        if (difference < 0)
        {
            if (SLS.Data.GameData.TotalCoinsAmount.Value >= SLS.Data.GameData.SelectCardPrice.Value)
            {
                SoundController.Instance.PlayEffectSound(putCardSound);
                OnCardSelected?.Invoke(_cardItemView);

                transform.DOMove((Vector2)_playTable.position, 0.2f)
                .SetEase(Ease.Linear)
                .OnComplete(SetRandomRotation);
            }
            else
            {
                DiscardCard();
            }
        }
        else
        {
            DiscardCard();
        }
    }

    private void DiscardCard()
    {
        SoundController.Instance.PlayEffectSound(discardCardSound);
        OnCardDiscarded?.Invoke(_cardItemView);
        transform.DOMove(new Vector2(-1f, -6f), 0.2f)
                .SetEase(Ease.Linear);
    }

    private void SetRandomRotation()
    {
        var zRotation = UnityEngine.Random.Range(-90f, 90f);
        transform.rotation = Quaternion.Euler(0f, 0f, zRotation);
    }

    private void CheckSuperVisionAbility(bool isActivated)
    {
        _isSuperVision = isActivated;
    }

    private void SuperVision()
    {
        if (_isSuperVision)
        {
            if (_cardItemView.Card.CardType != Cards.BlueJoker)
            {
                _cardItemView.ChangeIcon();
            }
        }
    }

    private void GetMousePosition()
    {
        if (_isClickDown)
        {
            _mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }
    }

    public void GetPlayTableTransform(Transform table)
    {
        _playTable = table;
    }
}

