using System.Collections;
using UnityEngine;

public class TigerSpawner : MonoBehaviour
{
    [SerializeField] private Transform spawnPoint;
    [SerializeField] private LittleTiger littleTigerPrefab;

    private void Start()
    {
        GameEvents.Instance.OnGameStarted += StartSpawnRoutine;
        GameEvents.Instance.OnGameStoped += StopAllCoroutines;
        GameEvents.Instance.OnGameLosed += StopAllCoroutines;
        GameEvents.Instance.OnGameLosed += DestroyTigers;
    }

    private void OnDestroy()
    {
        GameEvents.Instance.OnGameStarted -= StartSpawnRoutine;
        GameEvents.Instance.OnGameStoped -= StopAllCoroutines;
        GameEvents.Instance.OnGameLosed -= StopAllCoroutines;
        GameEvents.Instance.OnGameLosed -= DestroyTigers;
    }

    private void StartSpawnRoutine()
    {
        StartCoroutine(SpawnLittleTiger());
    }

    private IEnumerator SpawnLittleTiger()
    {
        yield return new WaitForSeconds(60);

        Instantiate(littleTigerPrefab.gameObject, spawnPoint);

        StartCoroutine(SpawnLittleTiger());
    }

    private void DestroyTigers()
    {
        spawnPoint.DestroyAllChild();
    }
}
