using System;

public class GameEvents : MonoSingleton<GameEvents>
{
    public event Action OnGameStarted;
    public event Action OnGameStoped;
    public event Action OnGameLosed;
    public event Action OnGameRestarted;
    public event Action OnHomePressed;
    public event Action OnPlayPressed;
    public event Action OnLifeLosed;
    public event Action<int> OnScoreIncreased;
    public event Action<int> OnScoreDecreased;
    public event Action OnJokersCombined;
    public event Action OnPutFirstCard;
    public event Action OnPutSecondCard;
    public event Action OnDelayPanelActivated;

    //Abilities
    public event Action OnDoubleScore;
    public event Action OnBonusLife;
    public event Action OnBonusTime;
    public event Action<bool> OnSuperVision;



    public void StartGame() => OnGameStarted?.Invoke();

    public void PlayerStopGame() => OnGameStoped?.Invoke();

    public void PlayerRestartGame() => OnGameRestarted?.Invoke();

    public void PlayerPressHome() => OnHomePressed?.Invoke();

    public void PlayerPressPlay() => OnPlayPressed?.Invoke();

    public void TimeIsUp() => OnGameLosed?.Invoke();

    public void AllLifesLosed() => OnGameLosed?.Invoke();

    public void PlayerLoseLife() => OnLifeLosed?.Invoke();

    public void IncreaseScore(int value) => OnScoreIncreased?.Invoke(value);

    public void DereaseScore(int value) => OnScoreDecreased?.Invoke(value);

    public void PlayerCombineJokers() => OnJokersCombined?.Invoke();

    public void PutFirstCard() => OnPutFirstCard?.Invoke();

    public void PutSecondCard() => OnPutSecondCard?.Invoke();

    public void ActivateDelayPanel() => OnDelayPanelActivated?.Invoke();


    //Abilities
    public void DoubleScoreActivated() => OnDoubleScore?.Invoke();

    public void BonusLifeActivated() => OnBonusLife?.Invoke();

    public void BonusTimeActivated() => OnBonusTime?.Invoke();

    public void SuperVisionActivated(bool active) => OnSuperVision?.Invoke(active);
}
