using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    [SerializeField] private Text timerText;
    private float _timeRemaining;
    public static bool IsInGame = false;

    private void Start()
    {
        SetTimeRemaining();

        GameEvents.Instance.OnGameStarted += SetInGame;
        GameEvents.Instance.OnJokersCombined += CombineJokersBonus;
        GameEvents.Instance.OnGameStoped += SetNotInGame;
        GameEvents.Instance.OnGameLosed += SetNotInGame;
        GameEvents.Instance.OnGameRestarted += SetTimeRemaining;
        GameEvents.Instance.OnBonusTime += AbilityBonus;
        GameEvents.Instance.OnHomePressed += SetNotInGame;
    }

    private void OnDestroy()
    {
        GameEvents.Instance.OnGameStarted -= SetInGame;
        GameEvents.Instance.OnJokersCombined -= CombineJokersBonus;
        GameEvents.Instance.OnGameStoped -= SetNotInGame;
        GameEvents.Instance.OnGameLosed -= SetNotInGame;
        GameEvents.Instance.OnGameRestarted -= SetTimeRemaining;
        GameEvents.Instance.OnBonusTime -= AbilityBonus;
        GameEvents.Instance.OnHomePressed -= SetNotInGame;
    }

    private void Update()
    {
        if (IsInGame)
        {
            if (_timeRemaining > 0)
            {
                _timeRemaining -= Time.deltaTime;
                DisplayTime(_timeRemaining);
            }
            else
            {
                _timeRemaining = 0;
                GameEvents.Instance.TimeIsUp();
            }
        }
    }

    private void SetTimeRemaining()
    {
        _timeRemaining = 5 * 60;
    }

    private void AddBonusTime(int bonusMinutes)
    {
        var bonusTime = bonusMinutes * 60;
        _timeRemaining += bonusTime;
    }

    private void CombineJokersBonus()
    {
        AddBonusTime(1);
    }

    private void AbilityBonus()
    {
        AddBonusTime(5);
    }

    private void DisplayTime(float timeToDisplay)
    {
        float minutes = Mathf.FloorToInt(timeToDisplay / 60);
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);

        timerText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }

    private void SetInGame()
    {
        IsInGame = true;
    }

    private void SetNotInGame()
    {
        IsInGame = false;
    }
}
