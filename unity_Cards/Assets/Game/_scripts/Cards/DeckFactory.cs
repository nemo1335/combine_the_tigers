using UnityEngine;

[CreateAssetMenu(fileName = "DeckFactory", menuName = "Deck/DeckFactory")]
public class DeckFactory : ScriptableObject
{
    [SerializeField] private CardItemView cardPrefab;

    public CardItemView Get(CardItem cardItem, Transform parent)
    {
        CardItemView instance;

        instance = Instantiate(cardPrefab, parent);

        instance.Initialize(cardItem);
        return instance;
    }
}
