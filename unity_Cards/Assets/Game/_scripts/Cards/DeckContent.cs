using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DeckContent", menuName = "Deck/DeckContent")]
public class DeckContent : ScriptableObject
{
    [SerializeField] private List<CardItem> cardsItem;
    public IEnumerable<CardItem> CardsItem => cardsItem;

    [ContextMenu("Shuffle")]
    public void Shuffle()
    {
        cardsItem.Shuffle();
    }
}
