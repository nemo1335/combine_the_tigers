using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeckManager : MonoBehaviour
{
    [SerializeField] private DeckFactory deckFactory;
    [SerializeField] private Transform playTable;
    [SerializeField] private Transform garbageCards;
    [SerializeField] private AudioClip combineTigersSound;
    [SerializeField] private AudioClip loseLifeSound;
    private CardSwipe _cardSwipe;
    private Cards _previousCardType;

    private void Start()
    {
        GameEvents.Instance.OnGameRestarted += DestroyGarbageCards;
    }

    private void OnDestroy()
    {
        GameEvents.Instance.OnGameRestarted -= DestroyGarbageCards;
    }

    public void CreateDeck(IEnumerable<CardItem> items, Transform deckParrent, List<CardItemView> deckList)
    {
        Clear(deckList);
        DestroyHalfGarbageCards();
        foreach (CardItem card in items)
        {
            CardItemView spawnedCard = deckFactory.Get(card, deckParrent);
            spawnedCard.cardSwipe.OnCardSelected += ChangeCardIcon;
            spawnedCard.cardSwipe.OnCardSelected += CheckSwipedCard;
            spawnedCard.cardSwipe.OnCardDiscarded += CheckDiscardedCard;
            spawnedCard.cardSwipe.GetPlayTableTransform(playTable);

            deckList.Add(spawnedCard);
        }
    }

    private void CheckSwipedCard(CardItemView cardItemView)
    {
        cardItemView.gameObject.transform.SetParent(garbageCards, true);

        if (cardItemView.Card.CardType != Cards.BlueJoker)
        {
            GameEvents.Instance.DereaseScore(SLS.Data.GameData.SelectCardPrice.Value);
        }

        if (_previousCardType == Cards.RedJoker)
        {
            if (cardItemView.Card.CardType == Cards.BlueJoker)
            {
                SoundController.Instance.PlayEffectSound(combineTigersSound);
                GameEvents.Instance.IncreaseScore(SLS.Data.GameData.CombineJokersPrice.Value);
                GameEvents.Instance.PlayerCombineJokers();
            }
            else
            {
                SoundController.Instance.PlayEffectSound(loseLifeSound);
                GameEvents.Instance.PlayerLoseLife();
            }
        }

        _previousCardType = cardItemView.Card.CardType;
    }

    private void CheckDiscardedCard(CardItemView cardItemView)
    {
        cardItemView.gameObject.transform.SetParent(garbageCards, true);
        if (cardItemView.Card.CardType != Cards.BlueJoker)
        {
            GameEvents.Instance.IncreaseScore(SLS.Data.GameData.SkipCardPrice.Value);
        }
        if (cardItemView.Card.CardType == Cards.RedJoker)
        {
            GameEvents.Instance.PlayerLoseLife();
        }
    }

    private void ChangeCardIcon(CardItemView cardItemView)
    {
        cardItemView.ChangeIcon();
    }

    private void Clear(List<CardItemView> deckList)
    {
        foreach (CardItemView card in deckList)
        {
            card.cardSwipe.OnCardSelected -= ChangeCardIcon;
            card.cardSwipe.OnCardSelected -= CheckSwipedCard;
            card.cardSwipe.OnCardDiscarded -= CheckDiscardedCard;

            Destroy(card.gameObject);
        }

        deckList.Clear();
    }

    private void DestroyGarbageCards()
    {
        if (garbageCards.childCount > 0)
        {
            garbageCards.DestroyAllChild();
        }
        _previousCardType = Cards.Common;
    }

    private void DestroyHalfGarbageCards()
    {
        for (int i = 0; i < garbageCards.childCount / 2; i++)
        {
            Destroy(garbageCards.GetChild(i).gameObject);
        }
    }
}
