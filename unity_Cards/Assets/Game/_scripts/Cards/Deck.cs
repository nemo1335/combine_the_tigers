using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Deck : MonoBehaviour
{
    [SerializeField] private DeckContent firstDeckContent;
    [SerializeField] private DeckContent secondDeckContent;
    [SerializeField] private DeckManager deckManager;
    [SerializeField] private Transform firstDeckParrent;
    [SerializeField] private Transform secondDeckParrent;
    private List<CardItemView> _firstDeckCards = new List<CardItemView>();
    private List<CardItemView> _secondDeckCards = new List<CardItemView>();

    private void Start()
    {
        CreateFirstDeck();
        CreateSecondDeck();

        GameEvents.Instance.OnGameRestarted += () =>
        {
            CreateFirstDeck();
            CreateSecondDeck();
        };
        GameEvents.Instance.OnPutFirstCard += SetTopFirstDeck;
        GameEvents.Instance.OnPutSecondCard += SetTopSecondDeck;
    }

    private void OnDestroy()
    {
        GameEvents.Instance.OnGameRestarted -= () =>
        {
            CreateFirstDeck();
            CreateSecondDeck();
        };
        GameEvents.Instance.OnPutFirstCard -= SetTopFirstDeck;
        GameEvents.Instance.OnPutSecondCard -= SetTopSecondDeck;
    }

    private void Update()
    {
        if (firstDeckParrent.childCount == 0)
        {
            CreateFirstDeck();
        }

        if (secondDeckParrent.childCount == 0)
        {
            CreateSecondDeck();
        }
    }

    private void SetTopCard(List<CardItemView> cards)
    {
        if (cards.Count != 0)
        {
            cards[cards.Count - 1].ActivateCollider();
            cards.RemoveAt(cards.Count - 1);
        }
    }

    private void CreateFirstDeck()
    {
        firstDeckContent.Shuffle();
        deckManager.CreateDeck(firstDeckContent.CardsItem.Cast<CardItem>(), firstDeckParrent, _firstDeckCards);
        SetTopFirstDeck();
    }

    private void CreateSecondDeck()
    {
        secondDeckContent.Shuffle();
        deckManager.CreateDeck(secondDeckContent.CardsItem.Cast<CardItem>(), secondDeckParrent, _secondDeckCards);
        SetTopSecondDeck();
    }

    private void SetTopFirstDeck()
    {
        SetTopCard(_firstDeckCards);
    }

    private void SetTopSecondDeck()
    {
        SetTopCard(_secondDeckCards);
    }
}
