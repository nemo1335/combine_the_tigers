using UnityEngine;

[CreateAssetMenu(fileName = "CardItem", menuName = "Deck/CardItem")]
public class CardItem : ScriptableObject
{
    [field: SerializeField] public Cards CardType { get; private set; }
    [field: SerializeField] public Sprite FrontIcon { get; private set; }
    [field: SerializeField] public Sprite BackIcon { get; private set; }

}
