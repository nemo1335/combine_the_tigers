using UnityEngine;

public class CardItemView : MonoBehaviour
{
    [SerializeField] private SpriteRenderer cardRenderer;
    [SerializeField] private SpriteRenderer frontIcon;
    [SerializeField] public CardSwipe cardSwipe;
    [SerializeField] private BoxCollider2D cardCollider;
    private static int orderInLayer = 0;
    private static int startOrderInLayer = 0;
    private Sprite _frontIcon;
    public CardItem Card { get; private set; }

    private void OnEnable()
    {
        cardCollider = GetComponent<BoxCollider2D>();
        cardCollider.enabled = false;
    }

    public void Initialize(CardItem card)
    {
        SetStartOrderInLayer();
        cardSwipe.OnCardSelected += SetPlayingOrderInlayer;
        frontIcon.sprite = card.BackIcon;
        _frontIcon = card.FrontIcon;
        Card = card;
    }

    private void OnDestroy()
    {
        cardSwipe.OnCardSelected -= SetPlayingOrderInlayer;
    }

    public void ActivateCollider()
    {
        cardCollider.enabled = true;
    }

    public void ChangeIcon()
    {
        frontIcon.sprite = _frontIcon;
    }

    public void SetPlayingOrderInlayer(CardItemView cardItemView)
    {
        cardRenderer.sortingOrder = orderInLayer;
        orderInLayer++;
        frontIcon.sortingOrder = orderInLayer;
        orderInLayer++;
    }

    private void SetStartOrderInLayer()
    {
        cardRenderer.sortingOrder = startOrderInLayer;
        startOrderInLayer++;
        frontIcon.sortingOrder = startOrderInLayer;
        startOrderInLayer++;
    }
}
