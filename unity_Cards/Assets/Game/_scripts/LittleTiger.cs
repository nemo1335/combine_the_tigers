using Unity.Mathematics;
using UnityEngine;

public class LittleTiger : MonoBehaviour
{
    [SerializeField] private float tigerSpeed = 1f;
    [SerializeField] private ParticleSystem dissapearTiger;
    [SerializeField] private AudioClip catchTigerSound;
    private Vector2 _startPosition;


    private void Start()
    {
        _startPosition = transform.position;
    }

    private void Update()
    {
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
        {
            Vector2 touchPos = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
            RaycastHit2D hit = Physics2D.Raycast(touchPos, Vector2.zero);

            if (hit.collider != null)
            {
                if (hit.collider.gameObject == this.gameObject)
                {
                    GameEvents.Instance.IncreaseScore(SLS.Data.GameData.CatchTigerPrice.Value);
                    SoundController.Instance.PlayEffectSound(catchTigerSound);
                    Instantiate(dissapearTiger, transform.position, quaternion.identity);
                    Destroy(this.gameObject);
                }
            }
        }
    }

    private void FixedUpdate()
    {
        if (Mathf.Abs(transform.position.x) - Mathf.Abs(_startPosition.x) <= 0)
        {
            Move();
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    private void Move()
    {
        if (Timer.IsInGame)
        {
            transform.Translate(Vector3.right * tigerSpeed * Time.deltaTime);
        }
    }
}
